
### IKAnalyzer-Lucene6.x

####特别感谢 林良益先生 提供分词源码 https://code.google.com/archive/p/ik-analyzer/ 

####Git ：https://git.oschina.net/OCO51/IKAnalyzer-Lucene6.x.git

###Solr配置示例：
---------------------------------------------------------------------------------------------------------
```
<fieldType name="text_ik" class="solr.TextField">   
  <analyzer type="index">
    <tokenizer class="org.wltea.analyzer.lucene.IKTokenizerFactory" useSmart="false" />
  </analyzer>
  <analyzer type="query">
    <tokenizer class="org.wltea.analyzer.lucene.IKTokenizerFactory" useSmart="true" />
  </analyzer>
</fieldType>
```


###<p>注：**useSmart=true：智能分词，useSmart=false：最细粒度分词**	</p>